from django.apps import AppConfig


class CmrappConfig(AppConfig):
    name = 'cmrapp'
