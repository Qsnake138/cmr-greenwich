from django.contrib import admin
from .models import Role, Faculty, Course, User

# Register your models here.
admin.site.register(Role)
admin.site.register(Faculty)
admin.site.register(Course)
admin.site.register(User)
