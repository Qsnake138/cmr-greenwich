from __future__ import unicode_literals
from django.utils import timezone
from django.db import models

# |
# | Role
# |
class Role(models.Model):
    name = models.CharField(max_length=50, default="")
    def __str__(self):
        return self.name

# |
# | User
# |
class User(models.Model):
    fname = models.CharField(max_length=255, default="")
    lname = models.CharField(max_length=255, default="")
    gender = models.NullBooleanField()
    id_role = models.ForeignKey(Role)
    def __str__(self):
        return self.fname + ' ' + self.lname

# |
# | Faculty
# |
class Faculty(models.Model):
    name = models.CharField(max_length=50, default="")
    pvc = models.ForeignKey(User, related_name='pvc')
    dlt = models.ForeignKey(User, related_name='dlt')
    def __str__(self):
        return self.name

# |
# | Course
# |
class Course(models.Model):
    code = models.CharField(max_length=50, default="")
    name = models.CharField(max_length=50, default="")
    due_date = models.DateTimeField(auto_now=True)
    cl = models.ForeignKey(User, related_name='cl')
    cm = models.ForeignKey(User, related_name='cm')
    def __str__(self):
        return self.name + ' ' + self.code

# |
# | Course_User
# |
class Course_User(models.Model):
    id_course = models.ForeignKey(Course)
    id_user = models.ForeignKey(User)

# |
# | Report
# |
class Report(models.Model):    
    status = models.IntegerField(default=None)
    aca_year = models.DateTimeField()
    stu_count = models.IntegerField(default=None)
    code_title = models.CharField(max_length=50, default="")
    cw_one = models.TextField(default="")
    cw_two = models.TextField(default="")
    cw_three = models.TextField(default="")
    cw_four = models.TextField(default="")
    id_cl = models.ForeignKey(User)